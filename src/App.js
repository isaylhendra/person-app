import { useState } from "react";
import './App.css';
import Person from './components/Person';
import Persons from "./components/Persons";

function App() {
  const [state, setState] = useState(
    [
      { nama: 'Hendra', role: 'Junior Programmer', nik: '10024918', usia: '30', img: 'hendra.png' },
      { nama: 'Ari', role: 'Senior Programmer', nik: '10000001', usia: '15', img: 'ari.png' },
      { nama: 'Herra', role: 'Super Senior Programmer', nik: '00000001', usia: '17', img: 'hera.png' },
    ]
  )

  return (
    <div className={"App"}>
      <h1>Selamat datang di Aplikasi Person</h1>
      <h2>Display From Re-use Component Person</h2>
      <Person
        id={1}
        nama={state[0].nama}
        role={state[0].role}
        nik={state[0].nik}
        profile={state[0].img}
        usia="30" />
      <Person
        id={2}
        nama={state[1].nama}
        role={state[1].role}
        nik={state[1].nik}
        profile={state[1].img}
        usia="29" />
      <Person
        id={3}
        nama={state[2].nama}
        role={state[2].role}
        nik={state[2].nik}
        profile={state[2].img}
        usia="17">
        What's up Bro ? by Mas {state[2].nama}
      </Person>
      <Persons
        state={state}
      />
    </div>
  );
}

export default App;