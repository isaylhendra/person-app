import './Person.css';
const Person = (props) => {
    const style = {
        display: 'inline-block',
        font: "inherit",
        width: '300px',
        color: '#fff',
        backgroundImage: "linear-gradient(#833ab4, #c13584)",
        borderRadius: '20px',
        height: 'auto',
        padding: '15px'
    }

    return (
        <>
            {/* <div style={style}> */}
            <div className="card container">
                <h1>Id: {props.id}</h1>
                <h1>{props.nama}</h1>
                <img src={require(`./assets/${props.profile}`).default} alt="Gambar Person" height="100%"/>
                <p>{props.role}</p>
                <p>{props.nik}</p>
                {/* <p>{Math.floor(Math.random() * 30)} years old</p> */}
                <p>{props.usia} years old</p>
                {props.children}
            </div>
        </>)
}

export default Person