import Person from "./Person";

const Persons = (props) => {
    return (
        <>
            <div>
                <h2>Display from component Persons</h2>
                {props.state.map((person, index) => {
                    return (
                        <Person key={index}
                            id={index}
                            nama={person.nama}
                            role={person.role}
                            nik={person.nik}
                            usia={person.usia}
                            profile={person.img}
                        />
                    )
                })}
            </div>
        </>
    );
}

export default Persons